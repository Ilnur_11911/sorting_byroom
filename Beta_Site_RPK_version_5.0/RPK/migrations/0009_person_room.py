# Generated by Django 3.0.5 on 2020-05-10 20:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RPK', '0008_auto_20200506_2044'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='room',
            field=models.IntegerField(max_length=6, null=True),
        ),
    ]
