from django.db import models
from django.conf import settings

class Person(models.Model):
    FIO = models.CharField(max_length=60,null=True)
    otvet0 = models.CharField(max_length=1,null=True)
    group = models.CharField(max_length=15,null=True)
    neighbour1 = models.CharField(max_length=60,null=True)
    neighbour2 = models.CharField(max_length=60,null=True)
    neighbour3 = models.CharField(max_length=60,null=True)
    neighbour4 = models.CharField(max_length=60,null=True)
    neighbour5 = models.CharField(max_length=60,null=True)
    neighbour6 = models.CharField(max_length=60,null=True)
    otvet1 = models.CharField(max_length=3,null=True)
    otvet2 = models.CharField(max_length=3,null=True)
    otvet3 = models.CharField(max_length=3,null=True)
    otvet4 = models.CharField(max_length=3,null=True)
    otvet5 = models.CharField(max_length=3,null=True)
    otvet6 = models.CharField(max_length=3,null=True)
    otvet7 = models.CharField(max_length=3,null=True)
    otvet8 = models.CharField(max_length=3,null=True)
    otvet9 = models.CharField(max_length=10,null=True)
    otvet10 = models.CharField(max_length=9,null=True)
    otvet11 = models.CharField(max_length=2,null=True)
    otvet12 = models.CharField(max_length=2,null=True)
    otvet13 = models.CharField(max_length=2,null=True)
    otvet14 = models.CharField(max_length=2,null=True)
    otvet15 = models.CharField(max_length=2,null=True)
    otvet16 = models.CharField(max_length=2, null=True)
    otvet21 = models.CharField(max_length=3, null=True)
    room = models.IntegerField(null=True)
    def __str__(self):
        return self.FIO



