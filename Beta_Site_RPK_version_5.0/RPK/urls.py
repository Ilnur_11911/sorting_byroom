from django.urls import path, re_path
from . import views
from django.conf.urls import url, include
from datetime import datetime, date, time
urlpatterns = [
path('create', views.create, name='create'),
path('index0.html', views.logining, name='start_list'),
path('index2.html', views.second_list, name='second_list'),
path('index1.html', views.third_list, name='third_list'),
path('index4.html', views.SortingByRoom, name='five_list'),
path('logout', views.log_out, name='logOUT'),
path('createDATA', views.file_read, name='file_read'),
path('newDATATIME',views.newDateTIME,name='newTime'),
url(r'^add-fromfile/$',views.file_read),
re_path(r'^', views.logining, name='logining'),
]