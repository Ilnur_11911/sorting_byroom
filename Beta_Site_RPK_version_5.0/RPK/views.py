from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import path, re_path
from RPK.models import Person
from itertools import *
from django.shortcuts import render, redirect
from django.template import context
from django.contrib import auth
from django.template.context_processors import csrf
from datetime import datetime, date, time



textF = open('time.txt', 'r')
dataF = textF.readline()
mas1 = dataF.split(',')
y = int(mas1[0])
m = int(mas1[1])
day = int(mas1[2])
d = datetime(y,m,day)
textF.close()

def newDateTIME(request):
    if (request.user.is_superuser == True):
        if request.method == "POST":
            textFF = open('time.txt','w')
            textFF.write(str(request.POST.get("year1"))+","+str(request.POST.get("month1"))+","+str(request.POST.get("day1")))
            textFF.close()
            return HttpResponseRedirect('/')
    else:
        HttpResponse("У вас нет прав")


def logining(request):
    if (datetime.now() < d):
        args = {}
        if not User.objects.all().filter(is_superuser=0): # Один раз и навсегда генерирует список авторизованных ребят, нужно научиться брать first
            f = open('1.txt', 'r', encoding="UTF-8")
            data = f.read().splitlines()
            for i in data:
                user = User.objects.create_user(i, ' ', 'randompassword123')
                user.save()
        if not Person.objects.all():
            f = open('1.txt', 'r', encoding="UTF-8")
            data = f.read().splitlines()
            for i in data:
                person = Person(FIO=i)
                person.save()
        if request.method == "POST":
            username = request.POST['username']
            user = authenticate(username=username, password='randompassword123')
            if user is not None:
                login(request, user)
                return redirect('index1.html')
            else:
                args['login_error'] = "Пользователь не найден"
                return render(request, 'RPK/index0.html', args)
        else:
            return render(request, 'RPK/index0.html', {})
    else:
        return SortingByRoom(request)


def start_list(request):
    return render(request, 'RPK/index0.html', {})

@login_required
def second_list(request):
    if (datetime.now() < d):
        return render(request, 'RPK/index2.html', {})
    else:
        return HttpResponseRedirect('/')

@login_required
def third_list(request):
    if (datetime.now() < d):
        return render(request, 'RPK/index1.html', {})
    else:
        return HttpResponseRedirect('/')

def fore_list(request):
    if (datetime.now() < d):
        return render(request, 'RPK/index3.html', {})
    else:
        return HttpResponseRedirect('/')

@login_required
def five_list(request):
    return render(request, 'RPK/index4.html', {})


def log_out(request):
    logout(request)
    return HttpResponseRedirect('/')

def file_read(request):
    if request.user.is_superuser == True:
        f = open('1.txt', 'r', encoding="UTF-8")
        data = f.read().splitlines()
        AuthPerson = Person.objects.all()
        n2 = len(AuthPerson)
        Users = User.objects.all()
        n1 = len(Users)
        mass = ['Данные обновлены (старые пользователи не пропали). Новые данные: ']
        for i in data:
            flag = False
            if AuthPerson:
                for k in range(n2):
                    if AuthPerson[k].FIO == str(i):
                        flag = True
            if flag == False:
                person = Person(FIO=i)
                person.save()
                mass += i+'; '
            flag = False
            if Users:
                for l1 in range(n1):
                    if Users[l1].username == str(i):
                        flag = True
            if flag == False:
                user = User.objects.create_user(i, ' ', 'randompassword123')
                user.save()
                flag = True
                for l2 in range(len(mass)):
                    if mass[l2] == str(i):
                        flag = False
                if flag == True:
                    mass += i + '; '
        return HttpResponse(mass)
    else:
        return HttpResponse('У вас нет прав')




def Matrix():
    elem = Person.objects.filter(otvet0='M')  # массив пацанов
    elem1 = Person.objects.filter(otvet0='W')  # массив девок
    n = len(elem)
    n1 = len(elem1)
    sum = 0
    sum1 = 0
    matrix = [[0] * n for _ in range(n)]
    matrix1 = [[0] * n1 for _ in range(n1)]
    for j in range(n): #строки j-персон в строке
        for i in range(j, n): # столбцы
            if (elem[j].group == elem[i].group): sum = sum + 10
            if (elem[j].otvet1 == elem[i].otvet1): sum = sum + 25  # Слушаете ли громко музыку ?
            if (elem[j].otvet2 == elem[i].otvet2): sum = sum + 25  # Играете ли Вы на музыкальных инструментах?
            if (elem[j].otvet3 == elem[i].otvet3): sum = sum + 15  # Много ли времени Вы уделяете компьютерным играм?
            if (elem[j].otvet4 == elem[i].otvet4): sum = sum + 35  # Можете ли Вы назвать себя спортсменом?
            if (elem[j].otvet5 == elem[i].otvet5 == "Да"): sum = sum + 55  # Готовы ли Вы заниматься учёбой в свободное время?
            if (elem[j].otvet5 != elem[i].otvet5): sum = sum + 25

            if (elem[j].otvet6 == elem[i].otvet6 == "Да"): sum = sum + 15  # курите
            if (elem[j].otvet6 == elem[i].otvet6 == "Нет"): sum = sum + 25  # курите
            if (elem[j].otvet7 == elem[i].otvet7): sum = sum + 15
            if (elem[j].otvet8 == elem[i].otvet8): sum = sum + 35
            if (elem[j].otvet9 == elem[i].otvet9 == "Интроверт"): sum = sum + 15
            if (elem[j].otvet10 == elem[i].otvet10 == ("Жаворонок" or "Сова")): sum = sum + 45
            if ((elem[j].otvet11 == "Да") and (elem[i].otvet4 == "Да")): sum = sum - 30
            if ((elem[i].otvet11 == "Да") and (elem[j].otvet4 == "Да")): sum = sum - 30
            if ((elem[j].otvet12 == "Да") and (elem[i].otvet3 == "Да")): sum = sum - 30
            if ((elem[i].otvet12 == "Да") and (elem[j].otvet3 == "Да")): sum = sum - 30
            if ((elem[j].otvet13 == "Да") and (elem[i].otvet2 == "Да")): sum = sum - 30
            if ((elem[i].otvet13 == "Да") and (elem[j].otvet2 == "Да")): sum = sum - 30
            if ((elem[j].otvet14 == "Да") and (elem[i].otvet6 == "Да")): sum = sum - 60
            if ((elem[i].otvet14 == "Да") and (elem[j].otvet6 == "Да")): sum = sum - 60
            if ((elem[j].otvet15 == "Да") and (elem[i].otvet9 == "Экстраверт")): sum = sum - 15
            if ((elem[i].otvet15 == "Да") and (elem[j].otvet9 == "Экстраверт")): sum = sum - 15
            if ((elem[j].otvet16 == "Да") and (elem[i].otvet9 == "Интроверт")): sum = sum - 15
            if ((elem[i].otvet16 == "Да") and (elem[j].otvet9 == "Интроверт")): sum = sum - 15

            if (elem[j].neighbour1 == elem[i].FIO): sum = sum + 900
            if (elem[i].neighbour1 == elem[j].FIO): sum = sum + 900
            if (elem[j].neighbour2 == elem[i].FIO): sum = sum + 900
            if (elem[i].neighbour2 == elem[j].FIO): sum = sum + 900
            if (elem[j].neighbour3 == elem[i].FIO): sum = sum + 900
            if (elem[i].neighbour3 == elem[j].FIO): sum = sum + 900
            if (elem[j].neighbour4 == elem[i].FIO): sum = sum - 950
            if (elem[i].neighbour4 == elem[j].FIO): sum = sum - 950
            if (elem[j].neighbour5 == elem[i].FIO): sum = sum - 950
            if (elem[i].neighbour5 == elem[j].FIO): sum = sum - 950
            if (elem[j].neighbour6 == elem[i].FIO): sum = sum - 950
            if (elem[i].neighbour6 == elem[j].FIO): sum = sum - 950
            matrix[j][i] = sum
            sum = 0
            if (i == j): matrix[j][i] = elem[j]

    for j in range(n1):  # строки j-персон в строке
        for i in range(j, n1):  # столбцыег
            if (elem1[j].group == elem1[i].group): sum1 = sum1 + 10
            if (elem1[j].otvet1 == elem1[i].otvet1): sum1 = sum1 + 25  # Слушаете ли громко музыку ?
            if (elem1[j].otvet2 == elem1[i].otvet2): sum1 = sum1 + 25  # Играете ли Вы на музыкальных инструментах?
            if (elem1[j].otvet3 == elem1[i].otvet3): sum1 = sum1 + 15  # Много ли времени Вы уделяете компьютерным играм?
            if (elem1[j].otvet4 == elem1[i].otvet4): sum1 = sum1 + 35  # Можете ли Вы назвать себя спортсменом?
            if (elem1[j].otvet5 == elem1[i].otvet5 == "Да"): sum1 = sum1 + 55  # Готовы ли Вы заниматься учёбой в свободное время?
            if (elem1[j].otvet5 != elem1[i].otvet5): sum1 = sum1 + 25

            if (elem1[j].otvet6 == elem1[i].otvet6 == "Да"): sum1 = sum1 + 15  #курите
            if (elem1[j].otvet6 == elem1[i].otvet6 == "Нет"): sum1 = sum1 + 25  # курите
            if (elem1[j].otvet7 == elem1[i].otvet7): sum1 = sum1 + 15
            if (elem1[j].otvet8 == elem1[i].otvet8): sum1 = sum1 + 35
            if (elem1[j].otvet9 == elem1[i].otvet9 == "Интроверт"): sum1 = sum1 + 15
            if (elem1[j].otvet10 == elem1[i].otvet10 == ("Жаворонок" or "Сова")): sum1 = sum1 + 45
            if ((elem1[j].otvet11 == "Да") and (elem1[i].otvet4 == "Да" )): sum1 = sum1 - 30
            if ((elem1[i].otvet11 == "Да") and (elem1[j].otvet4 == "Да")): sum1 = sum1 - 30
            if ((elem1[j].otvet12 == "Да") and (elem1[i].otvet3 == "Да")): sum1 = sum1 - 30
            if ((elem1[i].otvet12 == "Да") and (elem1[j].otvet3 == "Да")): sum1 = sum1 - 30
            if ((elem1[j].otvet13 == "Да") and (elem1[i].otvet2 == "Да")): sum1 = sum1 - 30
            if ((elem1[i].otvet13 == "Да") and (elem1[j].otvet2 == "Да")): sum1 = sum1 - 30
            if ((elem1[j].otvet14 == "Да") and (elem1[i].otvet6 == "Да" )): sum1 = sum1 - 60
            if ((elem1[i].otvet14 == "Да") and (elem1[j].otvet6 == "Да")): sum1 = sum1 - 60
            if ((elem1[j].otvet15 == "Да") and (elem1[i].otvet9 == "Экстраверт")): sum1 = sum1 - 15
            if ((elem1[i].otvet15 == "Да") and (elem1[j].otvet9 == "Экстраверт")): sum1 = sum1 - 15
            if ((elem1[j].otvet16 == "Да") and (elem1[i].otvet9 == "Интроверт")): sum1 = sum1 - 15
            if ((elem1[i].otvet16 == "Да") and (elem1[j].otvet9 == "Интроверт")): sum1 = sum1 - 15

            if (elem1[j].neighbour1 == elem1[i].FIO): sum1 = sum1 + 900
            if (elem1[i].neighbour1 == elem1[j].FIO): sum1 = sum1 + 900
            if (elem1[j].neighbour2 == elem1[i].FIO): sum1 = sum1 + 900
            if (elem1[i].neighbour2 == elem1[j].FIO): sum1 = sum1 + 900
            if (elem1[j].neighbour3 == elem1[i].FIO): sum1 = sum1 + 900
            if (elem1[i].neighbour3 == elem1[j].FIO): sum1 = sum1 + 900
            if (elem1[j].neighbour4 == elem1[i].FIO): sum1 = sum1 - 950
            if (elem1[i].neighbour4 == elem1[j].FIO): sum1 = sum1 - 950
            if (elem1[j].neighbour5 == elem1[i].FIO): sum1 = sum1 - 950
            if (elem1[i].neighbour5 == elem1[j].FIO): sum1 = sum1 - 950
            if (elem1[j].neighbour6 == elem1[i].FIO): sum1 = sum1 - 950
            if (elem1[i].neighbour6 == elem1[j].FIO): sum1 = sum1 - 950
            matrix1[j][i] = sum1
            sum1 = 0
            if (i == j): matrix1[j][i] = elem1[j]

    #return render(request, 'RPK/inex.html',     {'matrix': matrix,'matrix1':matrix1})
    return matrix, matrix1
@login_required
def create(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            username = request.user.username
            AuthPerson = Person.objects.all()
            n2 = len(AuthPerson)
            if AuthPerson:
                for i in range(n2):
                    if AuthPerson[i].FIO == str(username):
                        person = AuthPerson[i]
                        break
                    else:
                        person = Person()
            else:
                person = Person()

            person.FIO = str(username)
            person.group = request.POST.get("group")
            person.neighbour1 = request.POST.get("neighbour1")
            person.neighbour2 = request.POST.get("neighbour2")
            person.neighbour3 = request.POST.get("neighbour3")
            person.neighbour4 = request.POST.get("neighbour4")
            person.neighbour5 = request.POST.get("neighbour5")
            person.neighbour6 = request.POST.get("neighbour6")
            person.otvet0 = request.POST.get("otvet0")
            person.otvet1 = request.POST.get("otvet1")
            person.otvet2 = request.POST.get("otvet2")
            person.otvet3 = request.POST.get("otvet3")
            person.otvet4 = request.POST.get("otvet4")
            person.otvet5 =	request.POST.get("otvet5")
            person.otvet6 =	request.POST.get("otvet6")
            person.otvet7 =	request.POST.get("otvet7")
            person.otvet8 =	request.POST.get("otvet8")
            person.otvet9 =	request.POST.get("otvet9")
            person.otvet10 = request.POST.get("otvet10")
            person.otvet11 = request.POST.get("otvet11")
            person.otvet12 = request.POST.get("otvet12")
            person.otvet13 = request.POST.get("otvet13")
            person.otvet14 = request.POST.get("otvet14")
            person.otvet15 = request.POST.get("otvet15")
            person.otvet16 = request.POST.get("otvet16")
            person.otvet21 = request.POST.get("otvet21")
            person.save()
            if (request.POST.get('otvet22') == "Да"):
                request.user.is_active = False
                request.user.save()
            logout(request)
        else:
            return HttpResponseRedirect('/')
    return render(request, 'RPK/index3.html', {})

def Delete(graph, ind):
    for i in graph:
        i.pop(ind)
    graph.pop(ind) # ##DeleteUserFromMatrix
#Return sum of links of the user
def Sum(i):
    sum=0
    for j in i:
        if(type(j)==int):
            sum+=j
    return sum
#Change matrix a little bit
def ChangeGraph(graph):
    for i in range(0,len(graph)):
        for j in range(i+1,len(graph[i])):
            graph[j][i] = graph[i][j]
    return graph
#return sum of links, that goes out of room
def OutLinksSum(graph, podgraph):
    sum = 0
    for i in graph:
        for j in range(0,len(i)):
            if(type(i[j])!=str and i in podgraph):
                if(not(graph[j] in podgraph)):
                    sum+=i[j]
    return sum
#Transform line of graph into the object of Person
def GetStudentFromMatrixLine(line):
    for i in line:
        if(type(i) != int):
            return i
#get n rommates
def GetRoommates(graph, count):
    min = 100000
    res = []
    result = []
    for i in combinations(graph, count):
        if (OutLinksSum(graph, i) < min):
            result = i
            min = OutLinksSum(graph, i)

    for i in range(0, len(result)):
        for j in range(0, len(result[i])):
            if (type(result[i][j]) != int):
                res.append(result[i][j])
    for j in result:
        Delete(graph, graph.index(j))
    return res
def GetRoommatesPeek(graph, count):
    min = 100000
    res = []
    result = []
    for i in combinations(graph, count):
        if (OutLinksSum(graph, i) < min):
            result = i
            min = OutLinksSum(graph, i)

    for i in range(0, len(result)):
        for j in range(0, len(result[i])):
            if (type(result[i][j]) != int):
                res.append(result[i][j])
    return res
def is_there_disabled(students):
    result = False
    for i in students:
        if i.otvet21 == 'Да':
            result = True
    return result

def SortingByRoom(request):
    if (datetime.now() > d):
        args = {}
        rooms = [105, 109, 203, 204, 205, 206, 207, 208, 211, 212, 213, 218, 219, 220, 303, 304, 305, 306, 307, 308,
                 311, 312, 313, 318, 319, 320, 403, 404, 405, 406, 407, 408, 411, 412, 413, 216, 316, 416]
        if (request.user.is_authenticated == False):
            if request.method == "POST":
                username = request.POST['username']
                user = authenticate(username=username, password='randompassword123')
                if user is not None:
                    login(request, user)
                    t = Person.objects.all()
                    finalresult = dict.fromkeys(rooms, [])
                    for i in t:
                        if finalresult[i.room]:
                            finalresult[i.room] = finalresult[i.room] + [i]
                        else:
                            finalresult[i.room] = [i]

                    data = {"Room": finalresult}

                    return render(request, "RPK/index4.html", {'data': data})

        if (Person.objects.all()[0].room is not None):
            t = Person.objects.all()
            finalresult = dict.fromkeys(rooms,[])
            for i in t:
                if finalresult[i.room]:
                    finalresult[i.room]=finalresult[i.room] + [i]
                else:
                    finalresult[i.room] = [i]

            data = {"Room": finalresult}

            return render(request, "RPK/index4.html", {'data': data})
        else:
            boys, girls = Matrix()
            boysleft=[]
            girlsleft=[]
            graphB = ChangeGraph(boys)
            graphG = ChangeGraph(girls)
            finalresult = dict()
            retrooms=[]
            i = len(rooms)-1
            end = 0
            while(len(graphG)>=4 and i>=end):
                if rooms[i] % 100 == 16 and is_there_disabled(GetRoommatesPeek(graphG,5))==False:
                    finalresult[rooms[i]] = GetRoommates(graphG,5)
                    i-=1
                elif rooms[i] % 100 != 16 and is_there_disabled(GetRoommatesPeek(graphG,4))==False:
                    finalresult[rooms[i]] = GetRoommates(graphG, 4)
                    i -= 1
                elif is_there_disabled(GetRoommatesPeek(graphG,5))==True and rooms[end] % 100 == 16:
                    finalresult[rooms[end]] = GetRoommates(graphG, 5)
                    end+=1
                elif is_there_disabled(GetRoommatesPeek(graphG,4))==True and rooms[end] % 100 != 16:
                    finalresult[rooms[end]] = GetRoommates(graphG, 4)
                    end += 1

            while(len(graphB)>=4 and i>=end):
                if rooms[i] % 100 == 16 and is_there_disabled(GetRoommatesPeek(graphB, 5)) == False:
                    finalresult[rooms[i]] = GetRoommates(graphB, 5)
                    i -= 1
                elif rooms[i] % 100 != 16 and is_there_disabled(GetRoommatesPeek(graphB, 4)) == False:
                    finalresult[rooms[i]] = GetRoommates(graphB, 4)
                    i -= 1
                elif is_there_disabled(GetRoommatesPeek(graphB, 5)) == True and rooms[end] % 100 == 16:
                    finalresult[rooms[end]] = GetRoommates(graphB, 5)
                    end += 1
                elif is_there_disabled(GetRoommatesPeek(graphB, 4)) == True and rooms[end] % 100 != 16:
                    finalresult[rooms[end]] = GetRoommates(graphB, 4)
                    end += 1

            if(len(graphB)<4 and i==end and len(graphG)<4 and len(graphG)>0 and len(graphB)>0):
                if(len(graphB)>len(graphG)):
                    finalresult[rooms[i]] = GetRoommates(graphB, len(graphB))
                    i-=1
                if (len(graphB) <= len(graphG)):
                    finalresult[rooms[i]] = GetRoommates(graphG, len(graphG))
                    i-=1
            elif(len(graphB)<4 and i>end and len(graphG)<4):
                if(len(graphB)>0):
                    finalresult[rooms[i]] = GetRoommates(graphB, len(graphB))
                    i-=1
                if(len(graphG)>0):
                    finalresult[rooms[i]] = GetRoommates(graphG, len(graphG))
                    i-=1
            elif((len(graphB) > 0 or len(graphG) > 0) and i < end):
                for j in graphB:
                    boysleft.append(GetStudentFromMatrixLine(j))
                for j in graphG:
                    girlsleft.append(GetStudentFromMatrixLine(j))
            for j in range(end, i+1):
                retrooms.append(rooms[j])
            for room in finalresult:
                for student in finalresult[room]:
                    student.room = room
                    student.save()
            data = {"Room": finalresult, "roomsleft": retrooms, "boysleft": boysleft, "girlsleft": girlsleft}
            return render(request,"RPK/index4.html", {'data': data})
    else:
        return HttpResponseRedirect('/')
